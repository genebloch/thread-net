﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Mail;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.Common.Mail;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        public LikeService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<ICollection<UserDTO>> GetUsersThatLikeThePost(int id)
        {
            ICollection<User> users = new List<User>();

            var likes = await _context.PostReactions.Where(p => p.PostId == id).ToListAsync();

            foreach (PostReaction like in likes)
            {
                users.Add(_context.Users.First(u => u.Id == like.UserId));
            }

            return _mapper.Map<ICollection<UserDTO>>(users);
        }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);

            if (likes.Any())
            {
                _context.PostReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                return;
            }

            _context.PostReactions.Add(new PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();

            await LikeNotification(reaction);
        }

        public async Task LikeComment(NewReactionDTO reaction)
        {
            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId);

            if (likes.Any())
            {
                _context.CommentReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                return;
            }

            _context.CommentReactions.Add(new CommentReaction
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }

        private async Task LikeNotification(NewReactionDTO reaction)
        {
            var post = _context.Posts.First(p => p.Id == reaction.EntityId);
            var sender = _context.Users.First(u => u.Id == reaction.UserId);
            var recipient = _context.Users.First(u => u.Id == post.AuthorId);

            var mail = new MailMessageDTO()
            {
                UserName = sender.UserName,
                DestinationMail = recipient.Email,
                Body = $"{post.Body}"
            };

            await new MailService().SendMail(mail, $"{sender.UserName} liked your post");
        }
    }
}
