﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Mail;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.Mail;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task UpdatePost(PostEditDTO dto)
        {
            var postEntity = _context.Posts.First(p => p.Id == dto.PostId);

            bool isOwner = postEntity.AuthorId == dto.AuthorId;
            if (!isOwner) throw new Exception("You can edit only your posts");

            postEntity.Body = dto.Body;

            await _context.SaveChangesAsync();
        }

        public async Task SharePost(MailMessageDTO messageDTO)
        {
            await new MailService().SendMail(messageDTO, $"{messageDTO.UserName} shared post with you");
        }

        public async Task DeletePost(int postId, int userId)
        {
            var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == postId);

            if (post == null) throw new NotFoundException(nameof(Post), postId);
            if (post.AuthorId != userId) throw new Exception("You can delete only your posts");

            //dependencies removing
            var postReactions = await _context.PostReactions.Where(r => r.PostId == postId).ToListAsync();
            foreach (PostReaction reaction in postReactions)
            {
                _context.PostReactions.Remove(reaction);
            }

            var comments = _context.Comments.Where(c => c.PostId == postId);
            foreach (Comment comment in comments)
            {
                var commentReactions = await _context.CommentReactions.Where(r => r.CommentId == comment.Id).ToListAsync();
                foreach (CommentReaction reaction in commentReactions)
                {
                    _context.CommentReactions.Remove(reaction);
                }

                _context.Comments.Remove(comment);
            }

            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();
        }
    }
}
