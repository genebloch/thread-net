﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<ICollection<UserDTO>> GetUsersThatLikedTheComment(int id)
        {
            var users = new List<User>();

            var likes = await _context.CommentReactions.Where(x => x.CommentId == id).ToListAsync();

            foreach (CommentReaction reaction in likes)
            {
                int userId = reaction.UserId;
                users.Add(_context.Users.First(u => u.Id == userId));
            }

            return _mapper.Map<ICollection<UserDTO>>(users);
        }

        public async Task UpdateComment(CommentEditDTO dto)
        {
            var commentEntity = _context.Comments.First(c => c.Id == dto.CommentId);

            bool isOwner = commentEntity.AuthorId == dto.AuthorId;
            if (!isOwner) throw new Exception("You can edit only your comments");

            commentEntity.Body = dto.Body;

            await _context.SaveChangesAsync();
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task DeleteComment(int commentId, int userId)
        {
            var comment = await _context.Comments.FirstOrDefaultAsync(u => u.Id == commentId);

            var isOwner = _context.Posts.FirstOrDefault(p => p.Id == comment.PostId).AuthorId == userId;

            if (comment == null) throw new NotFoundException(nameof(Comment), commentId);
            if (comment.AuthorId != userId && !isOwner) throw new Exception("You can delete only your comments and comments on your posts");

            //dependencies removing
            var reactions = await _context.CommentReactions.Where(r => r.CommentId == commentId).ToListAsync();
            foreach (CommentReaction reaction in reactions)
            {
                _context.CommentReactions.Remove(reaction);
            }

            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();
        }
    }
}
