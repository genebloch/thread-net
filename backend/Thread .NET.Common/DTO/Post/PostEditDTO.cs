﻿namespace Thread_.NET.Common.DTO.Post
{
    public sealed class PostEditDTO
    {
        public int PostId { get; set; }
        public int AuthorId { get; set; }
        public string Body { get; set; }
    }
}
