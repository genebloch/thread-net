﻿namespace Thread_.NET.Common.DTO.Mail
{
    public class MailMessageDTO
    {
        public string UserName { get; set; }
        public string Body { get; set; }
        public string SenderMail { get; set; }
        public string SenderPassword { get; set; }
        public string DestinationMail { get; set; }
    }
}