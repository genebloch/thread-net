﻿namespace Thread_.NET.Common.DTO.Comment
{
    public sealed class CommentEditDTO
    {
        public int CommentId { get; set; }
        public int AuthorId { get; set; }
        public string Body { get; set; }
    }
}
