﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Mail;

namespace Thread_.NET.Common.Mail
{
    public class MailService
    {
        private readonly string _serviceMail = "threadNETservice@gmail.com";
        private readonly string _servicePassword = "randomPassword.87";

        public async Task SendMail(MailMessageDTO messageDTO, string subject)
        {
            messageDTO.SenderMail = _serviceMail;
            messageDTO.SenderPassword = _servicePassword;

            string userName = messageDTO.SenderMail.Split('@')[0];

            string smtpServer = "smtp.gmail.com";
            int port = 587;

            MailAddress from = new MailAddress(messageDTO.SenderMail, userName);
            MailAddress to = new MailAddress(messageDTO.DestinationMail);

            MailMessage message = new MailMessage(from, to)
            {
                Subject = subject,
                Body = messageDTO.Body
            };

            SmtpClient smtp = new SmtpClient(smtpServer, port)
            {
                EnableSsl = true,
                Credentials = new NetworkCredential(from.Address, messageDTO.SenderPassword)
            };
            await smtp.SendMailAsync(message);
        }
    }
}
