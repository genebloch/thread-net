export interface EditPost {
    postId: number;
    authorId: number;
    body: string;
}
