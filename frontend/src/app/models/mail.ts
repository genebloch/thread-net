export interface MailMessage {
    userName: string;
    body: string;
    destinationMail: string;
}