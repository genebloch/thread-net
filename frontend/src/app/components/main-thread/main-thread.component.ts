import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { User } from '../../models/user';
import { Subject } from 'rxjs';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AuthenticationService } from '../../services/auth.service';
import { PostService } from '../../services/post.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { EventService } from '../../services/event.service';
import { ImgurService } from '../../services/imgur.service';
import { NewPost } from '../../models/post/new-post';
import { switchMap, takeUntil } from 'rxjs/operators';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';
import { SnackBarService } from '../../services/snack-bar.service';

@Component({
    selector: 'app-main-thread',
    templateUrl: './main-thread.component.html',
    styleUrls: ['./main-thread.component.sass']
})
export class MainThreadComponent implements OnInit, OnDestroy {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];
    public isOnlyMine = false;

    public onlyMine: MatSlideToggleChange;
    public onlyFavorites: MatSlideToggleChange;
    public hideMine: MatSlideToggleChange;

    public sortIdentifier = 1; // 1: by date; 2: by popularity; 3: by comments amount

    public currentUser: User;
    public imageUrl: string;
    public imageFile: File;
    public post = {} as NewPost;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;

    public postHub: HubConnection;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private imgurService: ImgurService,
        private authDialogService: AuthDialogService,
        private eventService: EventService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.postHub.stop();
    }

    public ngOnInit() {
        this.registerHub();
        this.getPosts();
        this.getUser();

        this.eventService.userChangedEvent$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
            this.currentUser = user;
            this.post.authorId = this.currentUser ? this.currentUser.id : undefined;
        });
    }

    public getPosts() {
        this.loadingPosts = true;
        this.postService
            .getPosts()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.posts = this.cachedPosts = resp.body;
                },
                (error) => (this.loadingPosts = false)
            );
    }

    public sendPost() {
        const postSubscription = !this.imageFile
            ? this.postService.createPost(this.post)
            : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.body.data.link;
                    return this.postService.createPost(this.post);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.addNewPost(respPost.body);
                this.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public sort(value: string) {
        switch (value) {
            case '1':
                this.sortIdentifier = 1;
                this.sortPostsByDate(this.posts);
                break;
            case '2':
                this.sortIdentifier = 2;
                this.sortPostsByPopularity(this.posts);
                break;
            case '3':
                this.sortIdentifier = 3;
                this.sortPostsByCommentsAmount(this.posts);
                break;
        }
    }

    public searchByPosts() {
        var phrase = ((document.getElementById("searchForm") as HTMLInputElement).value);

        let posts: Post[] = [];

        let i = 0;
        while (i < this.cachedPosts.length) {
            let currentPost = this.cachedPosts[i];

            if (new RegExp(`([\\W])${phrase}([\\W])`, 'g').test(currentPost.body)) {
                posts.push(currentPost);
            }

            i++;
        }

        this.posts = posts;
    }

    public clearSearch() {
        (document.getElementById("searchForm") as HTMLInputElement).value = "";

        this.posts = this.cachedPosts;
    }

    public onlyMinePosts(event: MatSlideToggleChange) {
        if (this.hideMine != undefined) {
            this.hideMine.source.checked = false;
        }

        if (this.onlyFavorites != undefined) {
            this.onlyFavorites.source.checked = false;
        }

        if (event.checked) {
            this.isOnlyMine = true;
            this.posts = this.cachedPosts.filter((x) => x.author.id === this.currentUser.id);
        }
        else {
            this.isOnlyMine = false;
            this.posts = this.cachedPosts;
        }

        this.onlyMine = event;
    }

    public hideMyPosts(event: MatSlideToggleChange) {
        if (this.onlyMine != undefined) {
            this.onlyMine.source.checked = false;
        }

        if (this.onlyFavorites != undefined) {
            this.onlyFavorites.source.checked = false;
        }

        if (event.checked) {
            this.posts = this.cachedPosts.filter((x) => x.author.id != this.currentUser.id);
        }
        else {
            this.posts = this.cachedPosts;
        }

        this.hideMine = event;
    }

    public myLikes(event: MatSlideToggleChange) {
        if (this.onlyMine != undefined) {
            this.onlyMine.source.checked = false;
        }

        if (this.hideMine != undefined) {
            this.hideMine.source.checked = false;
        }

        if (event.checked) {
            let likes: Post[] = [];

            let i = 0;
            while (i < this.cachedPosts.length) {
                let j = 0;
                while (j < this.cachedPosts[i].reactions.length) {
                    if (this.cachedPosts[i].reactions[j].user.id === this.currentUser.id) {
                        likes.push(this.cachedPosts[i]);
                    }

                    j++
                }

                i++;
            }

            this.posts = likes;
        }
        else {
            this.posts = this.cachedPosts;
        }

        this.onlyFavorites = event;
    }

    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public registerHub() {
        this.postHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/post').build();
        this.postHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.postHub.on('NewPost', (newPost: Post) => {
            if (newPost) {
                this.addNewPost(newPost);
            }
        });
    }

    public addNewPost(newPost: Post) {
        switch (this.sortIdentifier) {
            case 1:
                if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
                    this.cachedPosts = this.sortPostsByDate(this.cachedPosts.concat(newPost));
                    if (!this.isOnlyMine || (this.isOnlyMine && newPost.author.id === this.currentUser.id)) {
                        this.posts = this.sortPostsByDate(this.posts.concat(newPost));
                    }
                }
                break;

            case 2:
                if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
                    this.cachedPosts = this.sortPostsByPopularity(this.cachedPosts.concat(newPost));
                    if (!this.isOnlyMine || (this.isOnlyMine && newPost.author.id === this.currentUser.id)) {
                        this.posts = this.sortPostsByPopularity(this.posts.concat(newPost));
                    }
                }
                break;

            case 3:
                if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
                    this.cachedPosts = this.sortPostsByCommentsAmount(this.cachedPosts.concat(newPost));
                    if (!this.isOnlyMine || (this.isOnlyMine && newPost.author.id === this.currentUser.id)) {
                        this.posts = this.sortPostsByCommentsAmount(this.posts.concat(newPost));
                    }
                }
                break;
        }
    }

    private getUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.currentUser = user));
    }

    private sortPostsByDate(array: Post[]): Post[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    private sortPostsByPopularity(array: Post[]): Post[] {
        return array.sort((a, b) => b.reactions.length - a.reactions.length);
    }

    private sortPostsByCommentsAmount(array: Post[]): Post[] {
        return array.sort((a, b) => b.comments.length - a.comments.length);
    }
}
