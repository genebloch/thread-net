import { Component, Input, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { PostService } from '../../services/post.service';
import { MailMessage } from '../../models/mail';
import { EditPost } from '../../models/post/editPost'

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public showEditPostContainer = false;
    public showComments = false;

    public newComment = {} as NewComment;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postService: PostService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public deletePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser()).toPromise();
            return;
        }

        this.postService.deletePost(this.post.id).subscribe(
            (resp) => {
                if (resp) {
                    window.location.reload();
                }
            },
            (error) => alert(error)
        );
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public likePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }

    public editPostContainer() {
        this.showEditPostContainer = !this.showEditPostContainer;
    }

    public editPost() {
        var newPostBody = ((document.getElementById("newPostBody") as HTMLInputElement).value);

        const post: EditPost = {
            postId: this.post.id,
            authorId: this.currentUser.id,
            body: newPostBody
        };

        this.postService.updatePost(post).subscribe(
            (resp) => {
                if (resp) {
                    window.location.reload();
                }
            },
            (error) => alert(error)
        );
    }

    public sharePost() {
        let destination = prompt("Share with (email):");

        const mail: MailMessage = {
            userName: this.currentUser.userName,
            body: this.post.body,
            destinationMail: destination
        };

        this.postService.sharePost(mail).toPromise();
    }

    public getUsersThatLikeThePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser()).toPromise();
            return;
        }

        this.postService.getUsersThatLikeThePost(this.post.id).toPromise().then(data => {
            let names = new Array<string>();

            for (let i = 0; i < data.body.length; i++) {
                names.push(data.body[i]['userName']);
            }

            alert(names.length != 0 ? `Likes:\n${names.join("\n")}` : "No likes");
        });
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
