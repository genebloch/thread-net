import { Component, Input, OnDestroy } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { CommentService } from 'src/app/services/comment.service';
import { LikeService } from '../../services/like.service';
import { User } from 'src/app/models/user';
import { AuthenticationService } from 'src/app/services/auth.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { EditComment } from 'src/app/models/comment/editComment';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnDestroy {
    @Input() public comment: Comment;

    public showEditCommentContainer = false;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private commentService: CommentService,
        private likeService: LikeService,
        private authService: AuthenticationService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public removeComment() {
        let commentId = this.comment.id;

        this.commentService.removeComment(commentId).subscribe(
            (resp) => {
                if (resp) {
                    window.location.reload();
                }
            },
            (error) => alert(error)
        );
    }

    public editCommentContainer() {
        this.showEditCommentContainer = !this.showEditCommentContainer;
    }

    public editComment() {
        var newCommentBody = ((document.getElementById("newCommentBody") as HTMLInputElement).value);

        const comment: EditComment = {
            commentId: this.comment.id,
            body: newCommentBody
        };

        this.commentService.updateComment(comment).subscribe(
            (resp) => {
                if (resp) {
                    window.location.reload();
                }
            },
            (error) => alert(error)
        );
    }

    public getUsersThatLikeTheComment() {
        this.commentService.getUsersThatLikeTheComment(this.comment.id).toPromise().then(data => {
            let names = new Array<string>();

            for (let i = 0; i < data.body.length; i++) {
                names.push(data.body[i]['userName']);
            }

            alert(names.length != 0 ? `Likes:\n${names.join("\n")}` : "No likes");
        });
    }

    public likeComment() {
        let currentUser: User;

        this.authService.getUser().subscribe(user => currentUser = user);

        this.likeService
            .likeComment(this.comment, currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }
}
