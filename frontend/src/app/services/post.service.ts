import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';
import { User } from '../models/user';
import { MailMessage } from '../models/mail';
import { EditPost } from '../models/post/editPost';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) { }

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/like`, reaction);
    }

    public getUsersThatLikeThePost(postId: number) {
        return this.httpService.getFullRequest<User[]>(`${this.routePrefix}/${postId}/likes`);
    }

    public sharePost(mail: MailMessage) {
        return this.httpService.postFullRequest(`${this.routePrefix}/share`, mail);
    }

    public updatePost(post: EditPost) {
        return this.httpService.putFullRequest(`${this.routePrefix}/editPost`, post);
    }

    public deletePost(id: number) {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
    }
}
