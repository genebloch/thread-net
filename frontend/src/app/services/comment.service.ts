import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { Comment } from '../models/comment/comment';
import { NewReaction } from '../models/reactions/newReaction';
import { User } from '../models/user';
import { EditComment } from '../models/comment/editComment';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) { }

    public createComment(post: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, post);
    }

    public removeComment(id: number) {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
    }

    public updateComment(comment: EditComment) {
        return this.httpService.putFullRequest(`${this.routePrefix}/editComment`, comment);
    }

    public getUsersThatLikeTheComment(commentId: number) {
        return this.httpService.getFullRequest<User[]>(`${this.routePrefix}/${commentId}/likes`);
    }

    public likeComment(reaction: NewReaction) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}/like`, reaction);
    }
}
